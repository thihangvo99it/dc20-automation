import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import java.awt.Desktop
import java.awt.Toolkit
import javax.imageio.ImageIO
import java.awt.Robot
import java.awt.image.BufferedImage
import java.sql.Array
import java.awt.Rectangle
import java.awt.event.KeyEvent
import com.kms.katalon.keyword.excel.ExcelKeywords
import com.kms.katalon.core.testdata.reader.ExcelFactory
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver as WebDriver
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook


Random rnd = new Random()
Date today = new Date()

String dateFolder = today.format('dd.MM.yyyy').replace('.', '/')
String nowTime = today.format('dd.MM.yyyy hh:mm:ss').replace(':', '.')
String link = '\\File Download\\Register.xlsx'

for(s=1;s<20;s++) {
	'open Browser'
	WebUI.openBrowser('https://demo.nopcommerce.com/')
	'maximize demo.nopcommerce '
	WebUI.maximizeWindow()
	
	WebDriver driver = DriverFactory.getWebDriver()
	String sScreenshotPath = (((((RunConfiguration.getProjectDir() + '/ScreenShotProduct')+ '/')+ dateFolder) + '/')+ nowTime) + '/'
	'read emai in file excel'
	String email =	CustomKeywords.'common.excel.readExcel'(link,s,1)
	
	'read password in file excel'
	String password =	CustomKeywords.'common.excel.readExcel'(link,s,2)
	
	'click Login'
	WebUI.click(findTestObject('Object Repository/Login/a_Login'))
	
	'input Email'
	WebUI.setText(findTestObject('Object Repository/Login/input_Text',[('value'):'Email']), email)
	
	'input Password'
	WebUI.setText(findTestObject('Object Repository/Login/input_Text',[('value'):'Password']), password)
	
	WebUI.click(findTestObject("Object Repository/Product/btn_addtocart",[("text"):"'Log in"]))
	
	'list menu Computers, Electronics, Apparel, Digital downloads, Books, Jewelry, Gift Cards'	
	List ListItem = CustomKeywords.'common.getData.getList'("//ul[@class='top-menu notmobile']/li/a")
	rndMenu= rnd.nextInt(ListItem.size())
	println rndMenu
	'click list Computers, Electronics, Apparel, Digital downloads, Books, Jewelry, Gift Cards'
	WebUI.click(findTestObject('Object Repository/Login/a_Navigate',[("Text"):ListItem[rndMenu]]))
	'check menu sublist '
	List<String> AloneList = new ArrayList()
	List<String> FallInLoveList = new ArrayList()
	for(int m =0;m<ListItem.size();m++){
		int test = 0
		List<String> litag1234 = driver.findElements(By.xpath("//ul[@class = 'top-menu notmobile']//descendant::li//a[text() = '" + ListItem.get(m) +  " ']//following-sibling::ul"))
		for (WebElement ele1234 : litag1234){
			String value1 = ele1234.tagName
			if(value1.equals("ul")){
				test = test +1
			}
		}
		if(test == 0){
			AloneList.add(ListItem.get(m))
		} 	
		else FallInLoveList.add(ListItem.get(m))
	}
		println(AloneList)
		println(FallInLoveList)
		
		int check = 0
		for(int ios = 0;ios<FallInLoveList.size();ios++){
			'menu sublist'
			if(FallInLoveList.get(ios) == ListItem[rndMenu]){				
				List ListSubCategories = CustomKeywords.'common.getData.getList'("//div[@class='item-grid']//h2/a")
				println ListSubCategories
				rndCategories= rnd.nextInt(ListSubCategories.size())
				WebUI.click(findTestObject('Object Repository/Product/Computers/div_Dessktops',[('Text'):ListSubCategories[rndCategories]]))
				
				List ListItemProduct1 = CustomKeywords.'common.getData.getList'('//div[@class="details"]//a')					
				String namproduct1 = "Levi's 511 Jeans"
				rndProduct1= rnd.nextInt(ListItemProduct1.size())
				println ListItemProduct1										
				if(ListItemProduct1.contains(namproduct1)) {
					'click product '
					WebUI.click(findTestObject('Object Repository/Product/Computers/div_clothings',[("Text"):ListItemProduct1[rndProduct1]]))
				}
				else {
					'click product '
					WebUI.click(findTestObject('Object Repository/Product/Computers/div_Clothing',[("Text"):ListItemProduct1[rndProduct1]]))
				}
				check = check+1
				break			
			}
		}
		
		if(check == 0){

			List ListItemProduct = CustomKeywords.'common.getData.getList'('//div[@class="details"]//a')
			String namproduct = "Levi's 511 Jeans"
			rndProduct= rnd.nextInt(ListItemProduct.size())
			println ListItemProduct
			
			if(ListItemProduct.contains(namproduct)) {
				'click product '
				WebUI.click(findTestObject('Object Repository/Product/Computers/div_clothings',[("Text"):ListItemProduct[rndProduct]]))
			}else {
				'click product '
				WebUI.click(findTestObject('Object Repository/Product/Computers/div_Clothing',[("Text"):ListItemProduct[rndProduct]]))
			}
		}
		
		List<String> ProducteList = CustomKeywords.'common.getData.getAttribute'('//div[@data-productid]', 'data-productid')
		println ProducteList
		'check product how many Add to cart'
		List<String> ProductidAddToCartButtonList = CustomKeywords.'common.getData.getAttribute'('//div[@class="page product-details-page"]//div[@data-productid="'+ProducteList.get(0)+'"]//div[@class="add-to-cart"]', 'class')
		println ProductidAddToCartButtonList

		if(ProductidAddToCartButtonList.size() == 0){
			List<String> nameSKU = CustomKeywords.'common.getData.getAttribute'('//div[@class="overview"]//div[@class="sku"]//span','id')
			println nameSKU
			List<String> nameSKU1 = CustomKeywords.'common.getData.getList'('//div[@class="overview"]//div[@class="sku"]//span[@id="'+nameSKU.get(1)+'"]')
			'take screenshot'
			WebUI.takeScreenshot((((((((sScreenshotPath +'/Error/')  + '/')+ s)+'_') + 'SS1') + '_') +nameSKU1) + '.png')
		}
		else if(ProductidAddToCartButtonList.size() != 1) {
				int spam = ProductidAddToCartButtonList.size()/2
				int productid = 1+ rnd.nextInt(spam)
				
				'click button Addtocart'
				WebUI.click(findTestObject('Object Repository/TC_AddToCart/btn_AddToCart',[('index'):productid]))
		}
		else if(ProductidAddToCartButtonList.size() == 1){
				List<String> ListProdutcAttributesId = CustomKeywords.'common.getData.getAttribute'('//div[@class="overview"]/div[@class="attributes"]//dd', 'id')
				println ListProdutcAttributesId
			
				List<String> ListProdutcAttributesClass = CustomKeywords.'common.getData.getAttribute'('//div[@class="overview"]/div', 'class')
				println ListProdutcAttributesClass
				List<String> ListSelect = CustomKeywords.'common.getData.getAttribute'('//div[@class="overview"]/div[@class="attributes"]//dd/*', 'data-attr')
			
				List<String> ListProdutcAttributesSelect = CustomKeywords.'common.getData.getTagname'('//div[@class="overview"]/div[@class="attributes"]//dd/*')		
				println(ListSelect)
				println(ListProdutcAttributesSelect)

	
				'Kiểm tra xem trong overview co the div chứa trường bắt buộc nhập hay không'
				for(int ov = 0;ov < ListProdutcAttributesClass.size();ov++){
				
				
					if(ListProdutcAttributesClass[ov] == "attributes rental-attributes"){
					
						String dateFo = today.format('MM.dd.yyyy').replace('.', '/')
						WebUI.setText(findTestObject('Object Repository/Product/Computers/div_datepicker'), dateFo )
						Calendar calendar = Calendar.getInstance();
						calendar.add(Calendar.DAY_OF_YEAR, 2);
						Date futureDateTime = calendar.getTime()
						a = futureDateTime.format('MM.dd.yyyy').replace('.', '/')
						WebUI.setText(findTestObject('Object Repository/Product/Computers/input_enđate'), a )							
					}			
					else if(ListProdutcAttributesClass[ov]=='attributes') {
							'list name value in card select'
							for(a=0;a<ListProdutcAttributesId.size();a++) {
								if(ListProdutcAttributesSelect.get(a).equals("select")) {
									List<String> ListProdutcSelectValue = CustomKeywords.'common.getData.getOptionAttrubute'('//div[@class="attributes"]//dl//dd//select[@data-attr="'+ListSelect.get(a)+'"]/option', 'value')
									println ListProdutcSelectValue
									rndProduct= rnd.nextInt(ListProdutcSelectValue.size())
									'select option select'
									WebUI.selectOptionByValue(findTestObject("Object Repository/Product/Computers/select_ProductAttribute",[("value"):ListSelect.get(a)]),ListProdutcSelectValue.get(rndProduct), false)
								}
								//list option card ul
								else if(ListProdutcAttributesSelect.get(a).equals("ul")) {
										List<String> ListProdutRadio = CustomKeywords.'common.getData.getOptionAttrubute'('//div[@class="attributes"]//dl//dd/ul[@data-attr="'+ ListSelect.get(a) +'"]/li/label', 'data-attr-value')
										println ListProdutRadio
										'click option radio'
										WebUI.click(findTestObject("Object Repository/Product/Computers/li_Raido",[("value"):ListSelect.get(a)]))
								}
				//input data in input box
								else if(ListProdutcAttributesSelect.get(a).equals("input")) {
					
										List<String> listInput= driver.findElements(By.xpath('//div[@class="attributes"]//dd//input'))
										for(ra=0;ra<listInput.size();ra++) {
											WebElement ele6 = listInput.get(ra)
											String value=ele6.getAttribute("id")
											'input data'
											WebUI.setText(findTestObject('Object Repository/Product/Computers/div_TextInput',[('value'):value]), 'I am Hang')
										}
								}	
							}

					}
			
					else if(ListProdutcAttributesClass[ov].equals("giftcard")){
							List <String> Listdiv = CustomKeywords.'common.getData.getTagname'('//div[@class="giftcard"]//div//*[2]')
							println Listdiv
							for(h=0;h<Listdiv.size();h++) {
								if(Listdiv.get(h).equals("input")) {
									List<String> listInput1= driver.findElements(By.xpath('//div[@class="giftcard"]//div//input'))
									List<String> ListGiftCard = new ArrayList()
									List<String> ListInputGiftCard = new ArrayList()
									for(h=0;h<listInput1.size();h++) {
										WebElement ele61 = listInput1.get(h)
										String valueType = ele61.getAttribute("type")
										String value1=ele61.getAttribute("id")
										ListGiftCard.add(value1)
										ListInputGiftCard.add(valueType)
									}

									println ListInputGiftCard
									println ListGiftCard
									for(d=0;d<ListGiftCard.size();d++) {			
										if(ListInputGiftCard.get(d).equals('text')) {				
											WebUI.setText(findTestObject('Object Repository/Product/Computers/div_giftcard',[('value'):ListGiftCard.get(d)]), 'Hang')
										}
										else {
											WebUI.setText(findTestObject('Object Repository/Product/Computers/div_giftcard',[('value'):ListGiftCard.get(d)]), 'Hang@gmail.com')
										}
									}
								}
							}
					}
				}
				'click Add to cart'
				WebUI.click(findTestObject('Object Repository/TC_AddToCart/btn_AddToCart',[('index'):'1']))
		}
		'get nam nam product and total'
		List<String> nameSKU2 = CustomKeywords.'common.getData.getAttribute'('//div[@class="overview"]//div[@class="sku"]//span','id')
		println nameSKU2
		List<String> nameSKU3 = CustomKeywords.'common.getData.getList'('//div[@class="overview"]//div[@class="sku"]//span[@id="'+nameSKU2.get(1)+'"]')
		nameProduct = WebUI.getText(findTestObject('Object Repository/Register/td_NameProduct'))
		total = WebUI.getText(findTestObject('Object Repository/Register/span_Total'))
		CustomKeywords.'common.excel.writeExcel'(link,nameProduct,s,3)
		CustomKeywords.'common.excel.writeExcel'(link,total,s, 4)

		result = CustomKeywords.'common.verify.verifyObjectPresent'(findTestObject('Object Repository/Product/p_message'))
		if(result==true) {
			re = CustomKeywords.'common.verify.verifyObjectPresent'(findTestObject('Object Repository/TC_AddToCart/div_message'))
			if(re==true) {
				WebUI.delay(1)
				'click Shopping cart'
				WebUI.click(findTestObject('Object Repository/Login/a_SCShipping',[('value'):'ico-cart']))
				'take screenshot'
				WebUI.takeScreenshot((((((((sScreenshotPath +'/Success/') + '/')+ s) + '_') + 'SS1') + '_') +nameSKU3) + '.png')
				CustomKeywords.'common.excel.writeExcel'(link,'Pass', s, 5)
			}
			else {
				message = WebUI.getText(findTestObject('Object Repository/Product/p_message'))
				CustomKeywords.'common.excel.writeExcel'(link,message, s, 6)
				'take screenshot'
				WebUI.takeScreenshot((((((((sScreenshotPath +'/Error/') + '/')+ s )+'_') + 'SS2') + '_') +nameSKU3) + '.png')
				CustomKeywords.'common.excel.writeExcel'(link,'Fail', s, 5)
			}
		}
		else {
			'click Shopping cart'
			WebUI.click(findTestObject('Object Repository/Login/a_SCShipping',[('value'):'ico-cart']))
			'take screenshot'
			WebUI.takeScreenshot((((((((sScreenshotPath +'/Error/') +'/')+ s )+'_') + 'SS2') + '_') +nameSKU3) + '.png')
			CustomKeywords.'common.excel.writeExcel'(link,'Fail', s, 5)
		
		}

}					
'close browser'

WebUI.closeBrowser()

Robot robot = new Robot()
WebUI.delay(2)
'open Excel'
String sDownloadPathSystem = RunConfiguration.getProjectDir() + '/File Download'
String excelFile = 'Register.xlsx'
ExcelPath = sDownloadPathSystem+'/'+ excelFile
String linkFile = ExcelPath.replace('/', '\\\\')
File file = new File(linkFile)
Desktop desktop = Desktop.getDesktop()
desktop.open(file)
WebUI.delay(7)
Workbook workbook = ExcelKeywords.getWorkbook(linkFile)
Sheet sheet = ExcelKeywords.getExcelSheet(workbook, "Sheet1")
int lastRow = sheet.getLastRowNum()
robot.keyPress(KeyEvent.VK_CONTROL)
robot.keyPress(KeyEvent.VK_HOME)
Thread.sleep(200)
robot.keyRelease(KeyEvent.VK_CONTROL)
robot.keyRelease(KeyEvent.VK_HOME)
for(x=0;x<lastRow;x++) {
'take screenshot file excel 1'
robot.keyPress(KeyEvent.VK_DOWN)
robot.keyPress(KeyEvent.VK_SHIFT)
robot.keyPress(KeyEvent.VK_SPACE)
robot.keyRelease(KeyEvent.VK_DOWN)
robot.keyRelease(KeyEvent.VK_SHIFT)
robot.keyRelease(KeyEvent.VK_SPACE)

Thread.sleep(200)
String format = "png"
String fileName = x+'_' +'Screenshot Excel File' +'.' + format
String sScreenshotPathExcel = RunConfiguration.getProjectDir() + '/ScreenshotExcel/'
Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize())
pathFilea = sScreenshotPathExcel.replace('/', '\\\\')
BufferedImage screenFullImage = robot.createScreenCapture(screenRect)
ImageIO.write(screenFullImage, format, new File(pathFilea + fileName))
WebUI.delay(1)
}
'Close Excel file'
robot.keyPress(KeyEvent.VK_ALT)
robot.keyPress(KeyEvent.VK_F4)
robot.keyPress(KeyEvent.VK_ALT)
robot.keyPress(KeyEvent.VK_F4)
robot.keyRelease(KeyEvent.VK_ALT)
robot.keyRelease(KeyEvent.VK_F4)
robot.keyRelease(KeyEvent.VK_ALT)
