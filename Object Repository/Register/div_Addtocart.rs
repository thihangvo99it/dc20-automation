<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Addtocart</name>
   <tag></tag>
   <elementGuidId>91fa6546-7851-4513-862f-4d4abaff701d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@class=&quot;item-grid&quot;] //button[@class=&quot;button-2 product-box-add-to-cart-button&quot;])[${index}]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
