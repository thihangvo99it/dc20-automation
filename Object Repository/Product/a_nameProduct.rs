<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_nameProduct</name>
   <tag></tag>
   <elementGuidId>1c30bec3-f988-41ab-b53a-3def6e98c5f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class=&quot;product-grid&quot;]//h2[@class=&quot;product-title&quot;]//a[contains(text(),'${value}')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
