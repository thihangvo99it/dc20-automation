<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_OptionDropdown</name>
   <tag></tag>
   <elementGuidId>5c88121a-f4dc-45df-8c45-30530f70bc4a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[normalize-space()='${text}']//parent::dt//following-sibling::dd[1]//select/option[contains(text(),'${value}')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
