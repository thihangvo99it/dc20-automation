<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_radio</name>
   <tag></tag>
   <elementGuidId>cff5dc7b-417d-4432-879f-4c7830424440</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[normalize-space()='${text}']//parent::dt//following-sibling::dd[1]//ul/li/label[contains(text(),'${value}')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
