<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_input</name>
   <tag></tag>
   <elementGuidId>0e44b45b-669a-46bd-8ba6-157ed7a75f4b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[normalize-space()=&quot;${text}&quot;]//parent::dt//following-sibling::dd[1]//input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
