<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Processor</name>
   <tag></tag>
   <elementGuidId>b01fefbe-0733-4cec-9c8d-c19fb1272c5f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@name=&quot;product_attribute_1&quot;]/option[contains(text(),'${value}')] </value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
